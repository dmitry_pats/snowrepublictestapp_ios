//
//  SRNewsItemTableViewCell.h
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 13.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRNewsItemTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UITextView *cellNewsText;
@property (nonatomic, weak) IBOutlet UIImageView *cellNewsImage;

@end
