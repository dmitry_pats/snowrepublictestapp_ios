//
//  SRServerManager.h
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 11.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface SRServerManager : AFHTTPSessionManager

+ (instancetype)sharedManager;

- (void)getTokenForUsername:(NSString *)login
				andPassword:(NSString *)password
		  andInstallationId:(NSString *)installationId
			   andCompanyId:(NSString *)companyId
			  andCheckAdmin:(NSString *)checkAdmin
					 succes:(void (^) (void))succes
					failure:(void (^) (NSError *))failure;

- (void)getAllInfoForLocale:(NSString *)locale
					succes:(void (^) (void))succes
				   failure:(void (^) (NSError *))failure;

@end
