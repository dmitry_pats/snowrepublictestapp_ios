//
//  SRDataManager.h
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 14.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SRDataManager : NSObject

+ (instancetype)sharedManager;

- (void)writeLogin:(NSString *)login andPassword:(NSString *)password andToken:(NSString *)token;
- (void)writeNews:(NSDictionary *)responseDictionary;

@end
