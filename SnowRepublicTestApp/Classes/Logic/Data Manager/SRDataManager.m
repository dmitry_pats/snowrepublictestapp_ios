//
//  SRDataManager.m
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 14.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import "SRDataManager.h"

//Logic
#import "SRAccountRealm.h"
#import "SRNewsItemRealm.h"

@implementation SRDataManager

+ (instancetype)sharedManager {
	static dispatch_once_t once;
	static SRDataManager *sharedManager;
	dispatch_once(&once, ^{
		sharedManager = [[self alloc] init];
	});
	return sharedManager;
}

-(void)writeLogin:(NSString *)login andPassword:(NSString *)password andToken:(NSString *)token {
	RLMRealm *realm = [RLMRealm defaultRealm];
	[realm beginWriteTransaction];
	RLMResults<SRAccountRealm *> *allAccount = [SRAccountRealm allObjects];
	if (allAccount) {
		[realm deleteObjects:allAccount];
	}
	SRAccountRealm *accountRealm = [[SRAccountRealm alloc] initWithLogin:login andPassword:password andToken:token];
	[realm addObject:accountRealm];
	NSError *writeError;
	[realm commitWriteTransaction:&writeError];
	if (writeError) {
		NSLog(@"Error - initialTid: %@", writeError.localizedDescription);
	}
}

-(void)writeNews:(NSDictionary *)responseDictionary {
	NSDictionary *dataDictionary = responseDictionary[@"data"];
	NSArray *newsArray = dataDictionary[@"news"];
	
	dispatch_async(dispatch_queue_create("background", 0), ^{
		NSMutableArray *newsRealmArray = [[NSMutableArray alloc] initWithCapacity:1];
		for (NSDictionary *newsItemDictionary in newsArray) {
			SRNewsItemRealm *newsItemRealm = [[SRNewsItemRealm alloc] initWithDictionary:newsItemDictionary];
			[newsRealmArray addObject:newsItemRealm];
		}
		RLMRealm *realm = [RLMRealm defaultRealm];
		[realm transactionWithBlock:^{
			[realm addOrUpdateObjectsFromArray:newsRealmArray];
		}];
	});
}

@end
