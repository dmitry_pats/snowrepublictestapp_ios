//
//  SRAccountRealm.m
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 14.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import "SRAccountRealm.h"

@implementation SRAccountRealm

-(instancetype)initWithLogin:(NSString *)login andPassword:(NSString *)password andToken:(NSString *)token {
	self = [super init];
	self.login = login;
	self.password = password;
	self.token = token;
	
	return self;
}

@end
