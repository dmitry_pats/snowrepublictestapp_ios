//
//  SRAccountRealm.h
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 14.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import <Realm/Realm.h>

@interface SRAccountRealm : RLMObject

@property NSString *login;
@property NSString *password;
@property NSString *token;

- (instancetype)initWithLogin:(NSString *)login andPassword:(NSString *)password andToken:(NSString *)token;

@end

RLM_ARRAY_TYPE(SRAccountRealm)
