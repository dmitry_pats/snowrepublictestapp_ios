//
//  SRNewsItemRealm.m
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 13.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import "SRNewsItemRealm.h"

@implementation SRNewsItemRealm

- (instancetype)initWithDictionary:(NSDictionary *)newsItemDictionary {
	
	self = [super initWithValue:newsItemDictionary];
	if (!self) return nil;

	if (newsItemDictionary[@"id"]) {
		self.identifier = newsItemDictionary[@"id"];
	}
	
	return self;
}

+ (NSString *)primaryKey{
	return @"identifier";
}

@end
