//
//  SRNewsItemRealm.h
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 13.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import <Realm/Realm.h>

@interface SRNewsItemRealm : RLMObject

@property NSNumber<RLMInt> *identifier;
@property NSString *title;
@property NSString *dateCreated;
@property NSString *text;
@property NSString *image;
@property NSNumber <RLMBool> *seen;
@property NSNumber <RLMDouble> *aspectRatio;

- (instancetype)initWithDictionary:(NSDictionary *)newsItemDictionary;

@end

RLM_ARRAY_TYPE(SRNewsItemRealm)
