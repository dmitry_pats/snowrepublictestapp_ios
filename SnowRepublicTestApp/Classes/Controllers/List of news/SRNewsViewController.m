//
//  SRNewsViewController.m
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 13.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import "SRNewsViewController.h"

//Logic
#import "SRServerManager.h"
#import "SRNewsItemRealm.h"

//Views
#import "SRNewsItemTableViewCell.h"

//Pods
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *kSRNewsItemTableViewCell = @"kSRNewsItemTableViewCell";

@interface SRNewsViewController ()

@property (nonatomic, strong) RLMNotificationToken *token;
@property (nonatomic, strong) RLMResults *dataNews;

@end

@implementation SRNewsViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	[self initialTable];
	[self initialRealmToken];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[self getNews];
}

- (void)dealloc {
	[self.token stop];
}

#pragma mark - Lazy Loading

- (void)initialRealmToken {
	__weak typeof(self) weakSelf = self;
	self.token = [[RLMRealm defaultRealm] addNotificationBlock:^(NSString *_Nonnull notification, RLMRealm *_Nonnull realm) {
		[weakSelf.tableView reloadData];
	}];
}

- (void)initialTable {
	[self.tableView registerNib:[UINib nibWithNibName:@"SRNewsItemTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:kSRNewsItemTableViewCell];
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	self.tableView.estimatedRowHeight = 64.0;
}

#pragma mark - Selector(s)

- (void)getNews {
	[[SRServerManager sharedManager] getAllInfoForLocale:@"en" succes:^(void) {
		NSLog(@"getAllInfoForToken - succes");
	} failure:^(NSError *error) {
		NSLog(@"%@", error);
	}];
}

#pragma mark - Getter(s)

-(RLMResults *)dataNews {
	RLMResults<SRNewsItemRealm *> *allNews = [SRNewsItemRealm allObjects];
	return allNews;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataNews.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	SRNewsItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"kSRNewsItemTableViewCell" forIndexPath:indexPath];
	
	SRNewsItemRealm *newsItemRealm = [self.dataNews objectAtIndex:indexPath.row];
	cell.cellNewsText.text = newsItemRealm.text;
	[cell.cellNewsImage sd_setImageWithURL:[NSURL URLWithString:newsItemRealm.image]];
    return cell;
}

@end
