//
//  SRRootViewController.h
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 14.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRRootViewController : UIViewController

- (void)showMessage:(NSString *)msg withTittle:(NSString *)tittle cancelTittle:(NSString *)cancel;

@end
