//
//  SRRootViewController.m
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 14.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import "SRRootViewController.h"

@interface SRRootViewController ()

@end

@implementation SRRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)showMessage:(NSString *)msg withTittle:(NSString *)tittle cancelTittle:(NSString *)cancel{
	UIAlertView * alert = [[UIAlertView alloc] initWithTitle:tittle
													 message:msg
													delegate:nil
										   cancelButtonTitle:cancel
										   otherButtonTitles:nil];
	[alert show];
}

@end
