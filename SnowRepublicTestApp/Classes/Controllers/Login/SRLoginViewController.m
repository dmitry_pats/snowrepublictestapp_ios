//
//  SRLoginViewController.m
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 11.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import "SRLoginViewController.h"

//Logic
#import "SRServerManager.h"
#import "SRAccountRealm.h"

static NSString *kShowNewsFromLogin = @"kShowNewsFromLogin";

@interface SRLoginViewController ()

@property (nonatomic, weak) IBOutlet UITextField *emailTF;
@property (nonatomic, weak) IBOutlet UITextField *passwordTF;

@end

@implementation SRLoginViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Selector(s)

- (IBAction)loginButtonTap:(id)sender {
	if (self.emailTF.text.length > 0 && self.passwordTF.text.length > 0) {
		__weak typeof(self) weakSelf = self;
		[[SRServerManager sharedManager] getTokenForUsername:self.emailTF.text andPassword:self.passwordTF.text andInstallationId:@"1" andCompanyId:@"1" andCheckAdmin:@"1" succes:^{
			__strong __typeof__(weakSelf) strongSelf = weakSelf;
			
			[strongSelf performSegueWithIdentifier:kShowNewsFromLogin sender:sender];
		} failure:^(NSError *error) {
			NSLog(@"%@", error);
			__strong __typeof__(weakSelf) strongSelf = weakSelf;
			[strongSelf showMessage:@"The email address or password is not valid." withTittle:@"Error" cancelTittle:@"OK"];
		}];
	}
	else {
		[self showMessage:@"Please enter your email or password." withTittle:@"Error" cancelTittle:@"OK"];
	}
}

@end
