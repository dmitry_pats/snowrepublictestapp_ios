//
//  AppDelegate.h
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 11.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

