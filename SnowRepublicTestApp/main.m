//
//  main.m
//  SnowRepublicTestApp
//
//  Created by Dzmitry Pats on 11.11.16.
//  Copyright © 2016 vironit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
